const express = require('express')
const multer = require('multer')
const bodyParser = require('body-parser')
const path = require('path')
const fs = require('fs')
const { check, validationResult } = require('express-validator')
const { isFilePosted, isAPDFFile } = require('./customValidators')
const { randomBytes, padString } = require('./utils')

const uploadedPath = path.resolve('uploads')
const htmlPath = path.resolve('html')
const defaultDocument = path.join(htmlPath, 'index.html')
const listeningPort = 8080

const app = express()
const upload = multer({
    dest: 'uploads/',
    limits: {
        fileSize: 10485760 // 10 MiB
    }
})

app.get('/', (req, res) => {
    res.status(200).sendFile(defaultDocument)
})

app.use(bodyParser.json())

app.post(
    '/upload',
    upload.single('fileToUpload'),
    check('description')
        .isString().withMessage('description must be a string').bail()
        .notEmpty().withMessage('description cannot be empty'),
    check('fileToUpload')
        .custom(isFilePosted).bail()
        .custom(isAPDFFile),
    async (req, res) => {
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            })
        }

        const filePath = path.resolve(req.file.path)
        const finalFileDir = path.join(uploadedPath)
        const finalFilePath = path.join(finalFileDir, `${padString(Date.now().toString('16'), 16, '0')}${randomBytes(24)}.pdf`)

        if (!fs.existsSync(finalFileDir)) {
            fs.mkdirSync(finalFileDir)
        }

        const readStream = fs.createReadStream(filePath)
        const writeStream = fs.createWriteStream(finalFilePath)

        readStream.pipe(writeStream)

        try {
            await new Promise((resolve, reject) => {
                writeStream.on('finish', resolve)
                writeStream.on('error', reject)
            })
        } catch (error) {
            fs.unlinkSync(filePath)
            fs.unlinkSync(finalFilePath)
            res.status(500).json({ error })
        }

        fs.unlinkSync(filePath)
        res.status(200).json({
            body: req.body,
            file: req.file
        })
    }
)

app.listen(listeningPort, () => console.log(`listening on port ${listeningPort}`))
