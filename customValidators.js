const FileType = require('file-type')
const path = require('path')
const fs = require('fs')

exports.isFilePosted = (value, { req }) => {
    if (!req.file) {
        throw new Error('fileToUpload is required')
    }

    return true
}

exports.isAPDFFile = async (value, { req }) => {
    const filePath = path.resolve(req.file.path)

    const defaultFileInfo = { ext: '', mime: '' }
    const fileInfo = await FileType.fromFile(filePath) || defaultFileInfo

    if (fileInfo.mime !== 'application/pdf') {
        fs.unlinkSync(filePath)
        throw new Error('file type not supported')
    }

    return true
}
