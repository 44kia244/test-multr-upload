const crypto = require('crypto')

exports.randomBytes = (n = 32) => {
    return crypto.randomBytes(n).toString('hex')
}
exports.padString = (str = '', minLength = 2, padChar = ' ') => {
    while (str.length < minLength) str = padChar + str
    return str
}